﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InterfacesNegocioCheckTime;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PojosCheckTime;
using PojosCheckTime.Response;

namespace CheckTimeWebApi.Controllers
{
    [Route("checkTime/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        public LoginService LoginService { get; }

        public LoginController(LoginService loginService) {
            LoginService = loginService;
        }

        [HttpPost]
        public ActionResult<UsuarioAutenticadoVO> Post(CredencialesUsuarioVO credenciales)
        {
            return LoginService.ValidarCredenciales(credenciales);
        }
    }
}