﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using PojosCheckTime.Response.Tarea;

namespace CheckTimeWebApi.Controllers
{
    [Route("checkTime/[controller]")]
    [ApiController]
    public class TareaController : ControllerBase
    {
        [HttpGet]
        public ActionResult<List<TareaVO>> Get()
        {
            List<TareaVO> tareas = new List<TareaVO>();
            TareaVO tarea = new TareaVO();
            tarea.Titulo = "Titulo 1";
            tarea.Codigo = "TAR001";
            tarea.Descripcion = "Tarea 1 Servicio";
            tarea.Estado = true;
            tarea.Modificacion = System.DateTime.Today;
            tareas.Add(tarea);
            return tareas;
        }
    }
}
