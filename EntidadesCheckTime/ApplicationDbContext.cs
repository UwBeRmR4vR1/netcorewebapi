﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace EntidadesCheckTime
{
    public class ApplicationDbContext : DbContext
    {

        private IConfiguration Configuration;

        public ApplicationDbContext(IConfiguration configuration) {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("defaultConnection")).EnableSensitiveDataLogging(true)
                .UseLoggerFactory(new LoggerFactory().AddConsole((categry, level) =>
                    level >= LogLevel.Debug, true
                ));
        }

    }
}
