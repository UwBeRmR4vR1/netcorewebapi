﻿using EntidadesCheckTime;
using InterfacesNegocioCheckTime;
using InterfacesRepositoriosCheckTime;
using PojosCheckTime;
using PojosCheckTime.Response;


namespace NegocioCheckTime
{
    public class LoginServiceImp : LoginService
    {
        public LoginRepository LoginRepository { get; }
        public readonly ApplicationDbContext DbContext;

        public LoginServiceImp(LoginRepository loginRepository, ApplicationDbContext dbContex) {
            LoginRepository = loginRepository;
            DbContext = dbContex;
        }

        public UsuarioAutenticadoVO ValidarCredenciales(CredencialesUsuarioVO credenciales)
        {
            return LoginRepository.ValidarCredenciales(credenciales);
        }
    }
}
