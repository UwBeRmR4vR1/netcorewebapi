﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PojosCheckTime.Response.Tarea
{
    public class TareaVO
    {
        public string Titulo { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public Boolean Estado { get; set; }
        public DateTime Modificacion { get; set; }
    }
}
