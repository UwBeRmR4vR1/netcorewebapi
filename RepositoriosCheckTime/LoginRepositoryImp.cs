﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterfacesRepositoriosCheckTime;
using PojosCheckTime;
using PojosCheckTime.Response;

namespace RepositoriosCheckTime
{
    public class LoginRepositoryImp : LoginRepository
    {
        
        public UsuarioAutenticadoVO ValidarCredenciales(CredencialesUsuarioVO credenciales)
        {
            UsuarioAutenticadoVO usuario = new UsuarioAutenticadoVO();
            usuario.UserName = credenciales.UserName;
            return usuario;
        }
    }
}
